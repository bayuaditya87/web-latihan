package com.radikom.web.latihan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebLatihanApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebLatihanApplication.class, args);
	}
}
